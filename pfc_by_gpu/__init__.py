#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .pfc_by_gpu import PowerSystem

__all__ = ["PowerSystem"]


def main():
    """Entry point for the application script"""
    print("Call your main application code here")
