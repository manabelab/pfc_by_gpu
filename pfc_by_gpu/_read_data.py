#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import os
import typing

import pandas as pd

from .constant import DATASET_DEFAULT, DATASET_LIST


def _read_data(dataset: str) -> typing.Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    data = _Data()
    if dataset in DATASET_LIST:
        csv_data_dir = os.path.join(os.path.dirname(__file__), "../data-set/" + dataset)
        _dir_list = [csv_data_dir]
    elif isinstance(dataset, str):
        _dir_list = [dataset]
    elif isinstance(dataset, list):
        _dir_list = dataset
    _csvfiles = []
    for _dir in _dir_list:
        _csvfiles.extend(glob.glob(_dir + "/**/[!_]*.csv", recursive=True))
    if len(_csvfiles) == 0:
        _dir = os.path.join(os.path.dirname(__file__), "../data-set/" + DATASET_DEFAULT)
        _csvfiles.extend(glob.glob(_dir + "/**/[!_]*.csv", recursive=True))
        _list = ", ".join(_dir_list)
        print(
            (
                "Since the data csv files "
                f"does not exist in the specified path ({_list}), "
                "the data set for operation check is used."
            )
        )
    for _csvfile in _csvfiles:
        _basename = os.path.splitext(os.path.basename(_csvfile))[0]
        if _basename.find("__") != -1:
            _basename = _basename[: _basename.find("__")]
        _newdf = pd.read_csv(_csvfile, skipinitialspace=True)
        _df = getattr(data, _basename, pd.DataFrame())
        _df = pd.concat([_df, _newdf], ignore_index=True)
        _df = _df.fillna(0)
        setattr(data, _basename, _df)

    data.bus.loc[data.bus.type == "PQ", "V"] = 1

    data.bus.set_index("name", inplace=True, verify_integrity=True)
    data.generation.set_index("name", inplace=True, verify_integrity=True)
    data.branch.set_index("name", inplace=True, verify_integrity=True)
    return data.bus, data.generation, data.branch


class _Data:
    def __init__(self):
        self.bus = pd.DataFrame()
        self.generation = pd.DataFrame()
        self.branch = pd.DataFrame()
