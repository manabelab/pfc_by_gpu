#!/usr/bin/env python
# -*- coding: utf-8 -*-


def _make_reference_PQ(ps):
    P = -ps.bus.loc[ps.index.sorted_bus].P
    P.loc[ps.index.bus_type.ref] = 0
    P.loc[ps.generation.bus] += list(ps.generation.P)

    Q = -ps.bus.loc[ps.index.sorted_bus].Q
    Q.loc[ps.index.bus_type.ref + ps.index.bus_type.PV] = 0

    return ps.np.array(P), ps.np.array(Q)
