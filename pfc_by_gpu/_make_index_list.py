#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import scipy


def _make_index_list(ps):
    index = _EmptyObj()
    index.bus_type, index.bus_type_i = _make_bus_type_index(ps)
    index.sorted_bus, index.sorted_bus_i, index.base_to_sorted = _make_sorted_bus_index(
        ps, index.bus_type, index.bus_type_i
    )
    index.gen_connection = _make_gen_connection_index(ps, index.sorted_bus)
    index.br_connection = _make_br_connection_index(ps, index.sorted_bus)
    return index


def _make_bus_type_index(ps):
    bus_type = _EmptyObj()
    bus_type_i = _EmptyObj()
    bus_type.ref = ps.bus.index[ps.bus.type == "ref"].values.tolist()
    bus_type.PV = ps.bus.index[ps.bus.type == "PV"].values.tolist()
    bus_type.PQ = ps.bus.index[ps.bus.type == "PQ"].values.tolist()
    bus_type_i.ref = np.where(ps.bus.type == "ref")[0].tolist()
    bus_type_i.PV = np.where(ps.bus.type == "PV")[0].tolist()
    bus_type_i.PQ = np.where(ps.bus.type == "PQ")[0].tolist()
    return bus_type, bus_type_i


def _make_sorted_bus_index(ps, bus_type, bus_type_i):
    sorted_bus = bus_type.ref + bus_type.PV + bus_type.PQ
    sorted_bus_i = bus_type_i.ref + bus_type_i.PV + bus_type_i.PQ
    index = (np.arange(len(ps.bus)), np.array(sorted_bus_i))
    array = scipy.sparse.csr_matrix((np.ones(len(ps.bus)), index))
    base_to_sorted = array.tocsc().indices

    return sorted_bus, sorted_bus_i, base_to_sorted


def _make_gen_connection_index(ps, sorted_bus):
    sorter = np.argsort(np.array(sorted_bus))
    bus = ps.generation.bus.to_numpy()

    gen_connection = sorter[np.searchsorted(sorted_bus, bus, sorter=sorter)]

    return gen_connection


def _make_br_connection_index(ps, sorted_bus):
    br_connection = _EmptyObj()
    sorter = np.argsort(np.array(sorted_bus))
    From = ps.branch.From.to_numpy()
    To = ps.branch.To.to_numpy()

    br_connection.From = sorter[np.searchsorted(sorted_bus, From, sorter=sorter)]
    br_connection.To = sorter[np.searchsorted(sorted_bus, To, sorter=sorter)]

    return br_connection


class _EmptyObj:
    def __init__(self):
        pass
