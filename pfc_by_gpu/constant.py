#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Define constant values."""
DATASET_LIST = [
    "2m4b1l",
    "2m4b2l",
    "2m4b2l_test",
    "3m9b1l",
    "3m9b2l",
    "IEEE14",
    "IEEE118",
    "GB",
    "case13659pegase",
]
DATASET_DEFAULT = "2m4b1l"
TOLERANCE = 1e-7
MAX_M = 20
MAX_PRINT_BUS = 100
