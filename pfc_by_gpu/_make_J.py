#!/usr/bin/env python
# -*- coding: utf-8 -*-


def _make_J(ps, V, I):
    V_diag = ps.sp.sparse.diags(V, format="csr")
    V_norm_diag = ps.sp.sparse.diags(V / abs(V), format="csr")
    I_diag = ps.sp.sparse.diags(I, format="csr")

    dS_dVa_part = I_diag - ps.Y * V_diag
    dS_dVa = 1j * V_diag * dS_dVa_part.conj()
    dS_dVm_part = ps.Y * V_norm_diag
    dS_dVm = V_norm_diag * I_diag.conj() + V_diag * dS_dVm_part.conj()

    dP_dVa = (dS_dVa[1:, 1:] + dS_dVa[1:, 1:].conj()) / 2
    dQ_dVa = (
        dS_dVa[1 + len(ps.index.bus_type.PV) :, 1:]
        - dS_dVa[1 + len(ps.index.bus_type.PV) :, 1:].conj()
    ) / 2j
    dP_dVm = (
        dS_dVm[1:, 1 + len(ps.index.bus_type.PV) :]
        + dS_dVm[1:, 1 + len(ps.index.bus_type.PV) :].conj()
    ) / 2
    dQ_dVm = (
        dS_dVm[1 + len(ps.index.bus_type.PV) :, 1 + len(ps.index.bus_type.PV) :]
        - dS_dVm[
            1 + len(ps.index.bus_type.PV) :, 1 + len(ps.index.bus_type.PV) :
        ].conj()
    ) / 2j

    JP = ps.sp.sparse.hstack([dP_dVa, dP_dVm])
    JQ = ps.sp.sparse.hstack([dQ_dVa, dQ_dVm])
    return ps.sp.sparse.vstack([JP, JQ], format="csr")
