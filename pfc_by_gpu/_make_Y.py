#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np


def _make_Y(ps):
    y = 1 / (ps.branch.r.to_numpy() + 1j * ps.branch.x.to_numpy())
    tap_ratio = ps.branch.tap_ratio.to_numpy()
    phase_shift = ps.branch.phase_shift.to_numpy()
    tap = tap_ratio * np.exp(1j * np.deg2rad(phase_shift))
    Yff = (
        (y + 1j * ps.branch.c.to_numpy() / 2)
        * ps.branch.para_num.to_numpy()
        / (tap * tap.conjugate())
    )
    Ytt = (y + 1j * ps.branch.c.to_numpy() / 2) * ps.branch.para_num.to_numpy()
    Yft = -y / tap.conjugate() * ps.branch.para_num.to_numpy()
    Ytf = -y / tap * ps.branch.para_num.to_numpy()
    Ys = ps.np.array(ps.bus.GS.to_numpy() + 1j * ps.bus.BS.to_numpy())

    C_from_index = (
        ps.np.arange(len(ps.branch)),
        ps.np.array(ps.index.br_connection.From),
    )
    C_from = ps.sp.sparse.csr_matrix(
        (ps.np.ones(len(ps.branch)), C_from_index,), (len(ps.branch), len(ps.bus)),
    )
    C_to_index = (ps.np.arange(len(ps.branch)), ps.np.array(ps.index.br_connection.To))
    C_to = ps.sp.sparse.csr_matrix(
        (ps.np.ones(len(ps.branch)), C_to_index,), (len(ps.branch), len(ps.bus)),
    )

    Y_row_index = ps.np.hstack((np.arange(len(ps.branch)), np.arange(len(ps.branch))))
    Y_col_index = ps.np.hstack(
        (np.array(ps.index.br_connection.From), np.array(ps.index.br_connection.To))
    )
    Y_from = ps.sp.sparse.csr_matrix(
        (ps.np.hstack((Yff, Yft)), (Y_row_index, Y_col_index)),
        (len(ps.branch), len(ps.bus)),
    )
    Y_to = ps.sp.sparse.csr_matrix(
        (ps.np.hstack((Ytf, Ytt)), (Y_row_index, Y_col_index)),
        (len(ps.branch), len(ps.bus)),
    )

    Y = (
        C_from.T * Y_from
        + C_to.T * Y_to
        + ps.sp.sparse.csr_matrix(
            (
                Ys,
                (
                    ps.np.array(ps.index.base_to_sorted),
                    ps.np.array(ps.index.base_to_sorted),
                ),
            ),
            (len(ps.bus), len(ps.bus)),
        )
    )
    return Y
