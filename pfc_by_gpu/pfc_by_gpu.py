#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Main script."""
import cmath
import math
import time

import cupy
import cupyx
import cupyx.scipy.sparse.linalg
import numpy
import scipy
import scipy.sparse.linalg

from ._make_index_list import _make_index_list
from ._make_J import _make_J
from ._make_reference_PQ import _make_reference_PQ
from ._make_Y import _make_Y
from ._read_data import _read_data
from .constant import DATASET_DEFAULT, MAX_M, MAX_PRINT_BUS, TOLERANCE


class PowerSystem:
    """Power system model for power flow calculation."""

    def __init__(self, dataset: str = DATASET_DEFAULT, use_gpu: bool = False,) -> None:
        """Constract power system model.

        Parameters
        ----------
        dataset : str, optional
            using model data, by default "2m4b"
        use_gpu : bool, optional
            Whether or not to use the GPU for computation, by default False
        """
        self.bus, self.generation, self.branch = _read_data(dataset)
        self.index = _make_index_list(self)
        self.use_gpu = use_gpu

        if use_gpu:
            self.np = cupy
            self.sp = cupyx.scipy
            self.linalg = cupyx.scipy.sparse.linalg
        else:
            self.np = numpy
            self.sp = scipy
            self.linalg = scipy.sparse.linalg

    def calc_powerflow(self) -> None:
        """Perform power flow calculation."""
        if hasattr(self.np, "cuda"):
            self.np.cuda.Stream.null.synchronize()
        time_start = time.time()

        self.Y = _make_Y(self)
        self.ref_P, self.ref_Q = _make_reference_PQ(self)

        V = self.np.array(
            self.bus.loc[self.index.sorted_bus, "V"].values, dtype=complex
        )

        m = 0
        max_dif = 1
        while max_dif > TOLERANCE and m < MAX_M:
            V_prev = self.np.copy(V)
            I = self.Y @ V
            J = _make_J(self, V, I)

            S = V * self.np.conjugate(I)
            P = S.real
            Q = S.imag

            dP = self.ref_P[1:] - P[1:]
            dQ = (
                self.ref_Q[1 + len(self.index.bus_type.PV) :]
                - Q[1 + len(self.index.bus_type.PV) :]
            )
            dPQ = self.np.hstack([dP, dQ])

            if self.use_gpu:
                lu = self.linalg.splu(J)
                V_def = lu.solve(dPQ.T)
            else:
                V_def = self.linalg.spsolve(J, dPQ.T)

            # PV node
            V[1 : 1 + len(self.index.bus_type.PV)] = V_prev[
                1 : 1 + len(self.index.bus_type.PV)
            ] * self.np.exp(1j * V_def[: len(self.index.bus_type.PV)])
            # PQ node
            V[1 + len(self.index.bus_type.PV) :] = (
                (
                    1
                    + V_def[len(self.bus) - 1 :]
                    / abs(V_prev[1 + len(self.index.bus_type.PV) :])
                )
                * V_prev[1 + len(self.index.bus_type.PV) :]
                * self.np.exp(
                    1j * V_def[len(self.index.bus_type.PV) : len(self.bus) - 1]
                )
            )

            difs = abs(V - V_prev)
            max_dif = max(difs)
            max_dif_index = int(difs.argmax())
            max_dif_bus = self.index.sorted_bus[max_dif_index]
            m += 1
            print(f"m = {m:2}:  max difference = {max_dif:g}({max_dif_bus})")

        if hasattr(self.np, "cuda"):
            self.np.cuda.Stream.null.synchronize()
        time_end = time.time()
        self.calc_time = time_end - time_start

        self.P = P
        self.Q = Q
        self.V = V

    def print_powerflow_calc_result(self) -> None:
        """Display the calculation result and execution time of power flow calc.."""
        print("\n--------------- Power Flow Calculation Result ---------------")
        for i in range(min(len(self.bus), MAX_PRINT_BUS)):
            ii = self.index.sorted_bus_i.index(i)
            print(
                (
                    f"{self.bus.iloc[i].name:>} :  "
                    f"P = {float(self.P[ii]):>7.4f}, "
                    f"Q = {float(self.Q[ii]):>7.4f}, "
                    f"V = {float(abs(self.V[ii])):>6.4f} "
                    f"({float(math.degrees(cmath.phase(self.V[ii]))):>7.4f} deg.)"
                )
            )
        if i == MAX_PRINT_BUS - 1:
            print(".\n.\n.")
        print("  -------------------------------------------------------  ")
        print(f"Calculation time         :  {self.calc_time:>25.10f}[sec]")
        print("-------------------------------------------------------------")
