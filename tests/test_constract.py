#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Test Module."""
import pytest

from pfc_by_gpu import PowerSystem


def test_constract():
    """Test for object constract."""
    ps = PowerSystem()
    assert len(ps.bus) == 4
    assert len(ps.generation) == 1
    assert len(ps.branch) == 4


def test_calculation_1():
    """Test for power flow calculation results at simple model."""
    ps = PowerSystem()
    ps.calc_powerflow()
    test_P = [0.36788255, 0.49999999, -0.54999989, -0.30000004]
    test_Q = [0.26469172, 0.09340881, -0.12999993, -0.17999993]
    test_V = [
        1.05 + 0.0j,
        1.0924151 + 0.12895441j,
        0.95869081 - 0.10838683j,
        0.98463791 - 0.00859585j,
    ]
    assert ps.P == pytest.approx(test_P, rel=1e-7)
    assert ps.Q == pytest.approx(test_Q, rel=1e-7)
    assert ps.V == pytest.approx(test_V, abs=1e-7)


def test_calculation_1_GPU():
    """Test for power flow calculation by GPU (case.1)."""
    ps = PowerSystem(use_gpu=True)
    ps.calc_powerflow()
    test_P = [0.36788255, 0.49999999, -0.54999989, -0.30000004]
    test_Q = [0.26469172, 0.09340881, -0.12999993, -0.17999993]
    test_V = [
        1.05 + 0.0j,
        1.0924151 + 0.12895441j,
        0.95869081 - 0.10838683j,
        0.98463791 - 0.00859585j,
    ]
    assert ps.P.get() == pytest.approx(test_P, rel=1e-7)
    assert ps.Q.get() == pytest.approx(test_Q, rel=1e-7)
    assert ps.V.get() == pytest.approx(test_V, abs=1e-7)


def test_calculation_2():
    """Test for power flow model calculation results with shunt bus and phase shift."""
    ps = PowerSystem(dataset="2m4b2l_test")
    ps.calc_powerflow()
    test_P = [0.40081272, 0.5, -0.54999999, -0.3]
    test_Q = [0.2847816, 0.08222279, -0.13, -0.17999999]
    test_V = [
        1.05 + 0.0j,
        1.08278811 + 0.19382959j,
        0.96360588 + 0.00097877j,
        0.98613005 + 0.05058935j,
    ]

    assert ps.P == pytest.approx(test_P, rel=1e-7)
    assert ps.Q == pytest.approx(test_Q, rel=1e-7)
    assert ps.V == pytest.approx(test_V, abs=1e-7)


def test_calculation_2_GPU():
    """Test for power flow calculation by GPU (case.1)."""
    ps = PowerSystem(dataset="2m4b2l_test", use_gpu=True)
    ps.calc_powerflow()
    test_P = [0.40081272, 0.5, -0.54999999, -0.3]
    test_Q = [0.2847816, 0.08222279, -0.13, -0.17999999]
    test_V = [
        1.05 + 0.0j,
        1.08278811 + 0.19382959j,
        0.96360588 + 0.00097877j,
        0.98613005 + 0.05058935j,
    ]

    assert ps.P.get() == pytest.approx(test_P, rel=1e-7)
    assert ps.Q.get() == pytest.approx(test_Q, rel=1e-7)
    assert ps.V.get() == pytest.approx(test_V, abs=1e-7)
