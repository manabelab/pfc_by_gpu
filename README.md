# Power flow calculation by GPU

Practical Power flow calculation programming used to teach the application of GPU calculations to power systems.

## Feature

- Power flow calculation using Newton-Raphson method.
- Enables to switch between CPU and GPU for some matrix operations.
- Easily convert and compare the computation time for each condition.
- A variety of power system models are available to study the effect of using the GPU on the scale of the problem.

## Requirements
- Windows 10 64bit or Windows 11
- Anaconda [How to install](https://docs.anaconda.com/anaconda/install/windows/)
- GPU made by NVIDIA
- NVIDIA STUDIO Driver or NVIDIA GAME READY Driver

## Install & run
### 1. Operation check

If not specified, optimization will be performed under the following conditions.

- Power system data: Small-scale system data set "2m4b1l" for operation check (4bus, 4branch)
- Processor used for matrix operations: CPU

1. Clone this registry.

2. Create the following script "main.py" in the same directory as this registry.

   ```python
   from pfc_by_gpu.pfc_by_gpu import powerflow

   ps = PowerSystem()
   ps.calc_powerflow()
   ps.print_powerflow_calc_result()
   ```

   <img src="docs/directories_1.drawio.png" width="300" alt="Construct of directories">

3. Create a virtual environment by reading the Anaconda environment configuration file "env.yml" in the root directory of this repository.

4. Run "main.py" on the virtual environment. There are two main methods as follows.

   - Start spyder, open "main.py", and execute it.

   - Open the console, move to the location of "main.py", and execute the following command.

     ```shell
     python main.py
     ```

5. The calculation results and calculation time are displayed on the console.

   ```
   m =  1:  max difference = 0.126582(bus4)
   m =  2:  max difference = 0.0122569(bus2)
   m =  3:  max difference = 0.000177058(bus2)
   m =  4:  max difference = 3.86526e-08(bus2)

   --------------- Power Flow Calculation Result ---------------
   bus1 :  P =  0.3679, Q =  0.2647, V = 1.0500 ( 0.0000 deg.)
   bus2 :  P = -0.5500, Q = -0.1300, V = 0.9648 (-6.4503 deg.)
   bus3 :  P = -0.3000, Q = -0.1800, V = 0.9847 (-0.5002 deg.)
   bus4 :  P =  0.5000, Q =  0.0934, V = 1.1000 ( 6.7323 deg.)
   -------------------------------------------------------
   Calculation time         :               0.0351803303[sec]
   -------------------------------------------------------------
   ```

### 2. Use GPU

Add the argument "use_gpu" to the constructor of the class "PowerSystem" and set it to True.

```python
from pfc_by_gpu.pfc_by_gpu import powerflow

ps = PowerSystem(use_gpu=True)
ps.calc_powerflow()
ps.print_powerflow_calc_result()
```

### 3. Change power system data set

Add the argument "dataset" to the constructor of the class "PowerSystem" and set the name of the dataset to be calculated.

The data sets provided are shown in the table below.

|      Name      | Buses | Branches | Generations |
|     :---:      | ----: |   ---:   |    ----:    |
|2m4b1l (default)|      4|         4|            1|
|2m4b2l          |      4|         4|            1|
|2m4b2l_test     |      4|         4|            1|
|3m9b1l          |      9|         9|            2|
|3m9b2l          |      9|         9|            2|
|IEEE14          |     14|        17|            4|
|IEEE118         |    118|       186|           53|
|GB              |   2224|      3207|          394|
|case13659pegase |  13659|     20467|         4092|

#### Example: Calculate "IEEE118" dataset
```python
from pfc_by_gpu.pfc_by_gpu import powerflow

ps = PowerSystem(dataset="IEEE118")
ps.calc_powerflow()
ps.print_powerflow_calc_result()
```

### 4. Calculate original power system data set
If you want to calculate for your own original power system dataset, create a new directory and put "branch.csv", "bus.csv", and Place "generation.csv".
Then, specify the directory path in the "dataset" argument.

If you want to know how to create scv files, read [dataset.md](./docs/dataset.md).

Note: Do not use an already prepared dataset name in the directory path.

   ```python
   from pfc_by_gpu.pfc_by_gpu import powerflow

   ps = PowerSystem(dataset="data")
   ps.calc_powerflow()
   ps.print_powerflow_calc_result()
   ```

   <img src="docs/directories_2.drawio.png" width="480" alt="Construct of directories when you want to calculate for your own original power system dataset">

## Author
### MANABE Yusuke

Ph.D. in Engineering

Representative Partner, [Manabe Lab LLC.](https://manabelab.com/)

manabe@manabelab.com

## Referenced Open Source Libraries
### [MATPOWER](https://matpower.org/)

#### 3-clause BSD license
Copyright (c) 1996-2020, Power Systems Engineering Research Center (PSERC)
and individual contributors.

[https://matpower.org/license/](https://matpower.org/license/)

### [PyPSA](https://github.com/PyPSA/pypsa)
#### MIT license
Copyright (c) 2015-2021, PyPSA Developers.

[https://github.com/PyPSA/PyPSA/blob/master/LICENSE.txt](https://github.com/PyPSA/PyPSA/blob/master/LICENSE.txt)
## License
Copyright (c) 2022 MANABE Yusuke

[MIT License](LICENSE)
