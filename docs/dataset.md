# Configuration of power system data set
- The dataset consists of three CVS files - branch.csv, bus.csv and generation.csv.

- These CSV files must be created in UTF-8 format.

## bus.scv
This is a CSV file that describes the bus information.

### Example: Data set "2m4b1l"
```csv
name,type,V,P,Q,GS,BS
bus1,ref,1.05,0,0,0,0
bus2,PQ,1,0.55,0.13,0,0
bus3,PQ,1,0.3,0.18,0,0
bus4,PV,1.1,0,0,0,0
```
| Index | Type                  | Description                                                                          |
| ----- | --------------------- | ------------------------------------------------------------------------------------ |
| name  | String, unique value  | Bus name                                                                             |
| type  | "ref", ”PV”, ”PQ”     | Bus type                                                                             |
| V     | Value                 | Voltage magnitude [p.u.]<br>If bus type is "PQ"、this value is ignored.              |
| P     | Value                 | Real power demand [p.u.]<br>If bus type is "ref"、this value is ignored.             |
| Q     | Value                 | Reactive power demand [p.u.]<br>If bus type is "ref" or "PV"、this value is ignored. |
| GS    | Value                 | Shunt conductance [p.u.]                                                             |
| BS    | Value                 | Shunt susceptance [p.u.]                                                             |

## branch.scv
This is a CSV file that describes the branch information.
### Example: Data set "2m4b1l"
```csv
name,From,To,para_num,r,x,c,tap_ratio,phase_shift
branch1,bus3,bus1,1,0.12,0.5,0.038404328,1,0
branch2,bus1,bus2,1,0.08,0.4,0.02826234,1,0
branch3,bus2,bus3,1,0.1,0.4,0.03056119,1,0
substation1,bus3,bus4,1,0,0.3,0,0.909090909,0
```
| Index       | Type                 | Description                                                                                |
| ----------- | -------------------- | ------------------------------------------------------------------------------------------ |
| name        | String, unique value | Branch name                                                                                |
| From        | Bus name             | "from" bus name                                                                            |
| To          | Bus name             | "to" bus name                                                                              |
| para_num    | Integer value        | Number of parallel branches                                                                |
| r           | Value                | Resistance [p.u.]                                                                          |
| x           | Value                | Reactance [p.u.]                                                                           |
| c           | Value                | Total line charging susceptance [p.u.]                                                     |
| tap_ratio   | Value                | Transformer off nominal turns ratio [ \|Vfrom\| / \|Vto\| ]                                          |
| phase_shift | Value                | Transformer phase shift angle [deg.]<br>If value is positive, this branches sift is delay. |

## generation.scv
This is a CSV file that describes the generation information.
### Example: Data set "2m4b1l"
```csv
name,bus,P
generation1,bus4,0.5
```
| Index | Type                 | Description                                           |
| ----- | -------------------- | ----------------------------------------------------- |
| name  | String, unique value | Generation name                                       |
| bus   | Bus name             | Connected bus name<br>Connected bus type most be "PV" |
| P     | Value                | Real power output [p.u.]                              |
